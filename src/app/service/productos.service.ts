import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';


const PRODUCTOS = 'productos';
const MIS_PRODUCTOS = 'mis_productos';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  productState = new BehaviorSubject(false);

  constructor(private storage: Storage, private router: Router) {
    if (!this.checkFav) {
      const lista: any[] = [];
      this.setFavoritos(lista);
      this.productState.next(true);
    }
  }

  initilizeFav() {
    const lista: any[] = [];
    this.setFavoritos(lista);
    this.productState.next(true);

  }
  checkFav() {
    this.storage.get(MIS_PRODUCTOS).then( res => {
      if (res) {
        this.productState.next(true);
      }
    });
  }
  public saveProductos(productos: any) { this.storage.set(PRODUCTOS, productos); }

 getProductos() { return this.storage.get(PRODUCTOS); }

  getFavoritos() { return this.storage.get(MIS_PRODUCTOS); }

  setFavoritos(lista: any[]) { this.storage.set(MIS_PRODUCTOS, lista); }

  addFavoritos(item: any, product: string) {
    this.storage.get(MIS_PRODUCTOS).then( res => {
      // console.log(res);
      if (res) {
        const variedadInfo = { producto: product, data: item,};
        res.push(variedadInfo);
        this.setFavoritos(res);
      }

    });
  }
  removeFavoritos(item: string) {
    this.storage.get(MIS_PRODUCTOS).then( res => {
      console.log(res);
      if (!res) {
        const lista: any[] = [];
        this.setFavoritos(lista);
      } else {
        const index: number = res.findIndex(x => x.data.Id == item);
        if (index !== -1) {
          res.splice(index, 1);
          this.setFavoritos(res);
        }
      }
    });
  }

  removeAllFav() {
    return this.storage.remove(MIS_PRODUCTOS).then(() => {
      this.productState.next(false);
      this.initilizeFav();
      this.router.navigate(['welcome']);
    });
  }
  getFavoritosProducto(id: string) {
    this.storage.get(MIS_PRODUCTOS).then( res => {
      return res.filter( x => x.producto === id);
    });
  }
}
