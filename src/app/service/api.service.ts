import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const apiUrl = 'http://134.209.55.25/';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private hpptClient: HttpClient) { }

  // Noticias

  getNewsServer() {
    const url = `${apiUrl}getNoticias`;
    return this.hpptClient.get(url);
  }

  // Location

  getDepartamentos() {
    const url = `${apiUrl}getDepartamentos`;
    return this.hpptClient.get(url);
  }
  getProvincias(id: string) {
    const url = `${apiUrl}getProvincias/${id}`;
    return this.hpptClient.get(url);
  }
  getCiudades(id: string) {
    const url = `${apiUrl}getCiudades/${id}`;
    return this.hpptClient.get(url);
  }
  getCiudad(id: string) {
    const url = `${apiUrl}getCiudad/${id}`;
    return this.hpptClient.get(url);
  }

  // Precio
  getPrecioVariedad(id: string) {
    const url = `${apiUrl}getPreciosProducto/${id}`;
    return this.hpptClient.get(url);
  }

  // Variedad
  getProductos() {
    const url = `${apiUrl}getProductos`;
    return this.hpptClient.get(url);
  }
  getVariedades(id: string) {
    const url = `${apiUrl}getVariedades/${id}`;
    return this.hpptClient.get(url);
  }

  // Clima
  getClima(id: string) {
    const url = `${apiUrl}getClima/${id}`;
    return this.hpptClient.get(url);
  }
  getClimaActual(id: string) {
    const url = `${apiUrl}getClimaActual/${id}`;
    return this.hpptClient.get(url);
  }
  getClimaSemana(id: string) {
    const url = `${apiUrl}getClimaSemana/${id}`;
    return this.hpptClient.get(url);
  }
}
