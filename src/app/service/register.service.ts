import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

const TOKEN_KEY = 'user-tkn';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  registerState = new BehaviorSubject(false);
  private mapper: any[] =
  [
    {json: 'nombre', nombre: 'Nombre:'},
    {json: 'ciudad', nombre: 'Ciudad: '},
  ];

  constructor(private storage: Storage, private platform: Platform, private router: Router) {
    this.platform.ready().then(() => {
      this.checkToken();
    });
  }

  // this function checks if there are tokens left
  checkToken() {
    this.storage.get(TOKEN_KEY).then( res => {
      if (res) {
        this.registerState.next(true);
      }
    });
  }

  register(name: string, city: any, province: string, departament: string) {
    const userInfo = {
      nombre: name,
      ciudad: city,
      provincia: province,
      departamento: departament,
    };

    return this.storage.set(TOKEN_KEY, userInfo).then(() => {
      this.registerState.next(true);
      this.router.navigate(['home']);
    });
  }

  unregister() {

    return this.storage.remove(TOKEN_KEY).then(() => {
      this.registerState.next(false);
      this.router.navigate(['welcome']);
    });
  }

  isRegister() {
    return this.registerState.value;
  }
  getUserInfo() {
    return this.storage.get(TOKEN_KEY);
  }
  getMapper(){
    return this.mapper;
  }
}
