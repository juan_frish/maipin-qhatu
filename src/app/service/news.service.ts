import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private hashtags: string[] = ['Perú', 'Quinua', 'quinua', 'turístico', 'orgánico',
  'orgánica', 'Puno', 'precio', 'nutrición', 'comercialización'];

    constructor(private apiService: ApiService) { }

    isHashtagWord(word: string) {
      for (let lib of this.hashtags) {
        if (word === lib) {
          return true;
        }
      }
      return false;
    }

    isNotWordInArray(word: string, words: string[]) {
      for (let w of words) {
        if (word === w) {
          return false;
        }
      }
      return true;
    }
  }
