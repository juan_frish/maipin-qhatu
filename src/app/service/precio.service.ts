import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrecioService {
  private mapper: any[] =
  [
    {json: 'Fecha', nombre: 'Fecha:', prefijo: ''},
    {json: 'Ciudad', nombre: 'Ciudad:', prefijo: ''},
    {json: 'precio_promedio', nombre: 'Precio promedio:', prefijo: 'S/'},
    {json: 'precio_max', nombre: 'Precio máximo', prefijo: 'S/'},
    {json: 'precio_min', nombre: 'Precio mínimo', prefijo: 'S/'}, 
  ];


  constructor() { }

  getMapper(): any[] {
    return this.mapper;
  }

}
