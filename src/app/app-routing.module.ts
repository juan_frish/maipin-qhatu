import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RegisterGuard } from './guard/register.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'welcome', loadChildren: './public/welcome/welcome.module#WelcomePageModule' },
  { path: 'register', loadChildren: './public/register/register.module#RegisterPageModule' },

  { path: 'home', loadChildren: './data/home/home.module#HomePageModule', canActivate: [RegisterGuard] },
  { path: 'news', loadChildren: './data/news/news.module#NewsPageModule', canActivate: [RegisterGuard] },

  { path: 'productos', loadChildren: './data/productos/productos.module#ProductosPageModule', canActivate: [RegisterGuard] },
  { path: 'precio', loadChildren: './data/precio/precio.module#PrecioPageModule', canActivate: [RegisterGuard] },

  { path: 'clima', loadChildren: './data/clima/clima.module#ClimaPageModule', canActivate: [RegisterGuard] },
  { path: 'perfil', loadChildren: './data/perfil/perfil.module#PerfilPageModule', canActivate: [RegisterGuard] },
  { path: 'editar', loadChildren: './data/perfil/editar/editar.module#EditarPageModule',  canActivate: [RegisterGuard] },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
