import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from '../../service/register.service';
import { LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  // variables
  registerForm: FormGroup;

  departamentos: any;
  provincias: any;
  ciudades: any;

  // constantes
  DEP_FLAG = true;
  PROV_FLAG = true;
  CIUDAD_FLAG = true;

  constructor(
    public formBuilder: FormBuilder, 
    private registerService: RegisterService,
    private loadingController: LoadingController,
    private api: ApiService
    ) {

    this.registerForm = formBuilder.group({
      nombre: ['', Validators.compose([
        Validators.maxLength(30),
        Validators.minLength(3),
        Validators.pattern('[a-zA-Z ]*'),
        Validators.required
      ])],
      departamento: ['', Validators.compose([
        Validators.required
      ])],
      provincia: ['', Validators.compose([
        Validators.required
      ])],
      ciudad: ['', Validators.compose([
        Validators.required
      ])]

  });
  }

  ngOnInit() {
    this.getAsyncDepartamentos();
  }

  onDepartamentoChange() {
    this.getAsyncProvincias(this.registerForm.value.departamento);
  }

  onProvinciaChange() {
    this.getAsyncCiudades(this.registerForm.value.provincia);
  }

  register() {
    if (this.registerForm.valid) {
      console.log(this.registerForm.value.ciudad);
      this.registerService.register(
        this.registerForm.value.nombre,
        this.registerForm.value.ciudad,
        this.registerForm.value.provincia,
        this.registerForm.value.departamento);
    }
  }

  // Manage Departamentos
  async getAsyncDepartamentos() {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getDepartamentos().subscribe( res => {
            this.departamentos = res;
            loading.dismiss();
            this.DEP_FLAG = false;
            }, err => {
              console.log(err);
              loading.dismiss();
            });

  }

  async getAsyncProvincias(id: string) {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getProvincias(id).subscribe( res => {
            this.provincias = res;
            loading.dismiss();
            this.PROV_FLAG = false;
            }, err => {
              console.log(err);
              loading.dismiss();
            });

  }

  async getAsyncCiudades(id: string) {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getCiudades(id).subscribe( res => {
            this.ciudades = res;
            loading.dismiss();
            this.CIUDAD_FLAG = false;
            }, err => {
              console.log(err);
              loading.dismiss();
            });

  }
}
