import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { NewsService } from '../../service/news.service';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

  noticias: any;
  hashtags: string[];
  bandera = true;

  constructor(private newsService: NewsService, private apiService: ApiService, private loadingController: LoadingController) {}

  ngOnInit() {

    this.getAsyncNews();
  }

  async getAsyncNews() {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.apiService.getNewsServer().subscribe( res => {
            this.noticias = res;
            loading.dismiss();
            this.bandera = false;
            }, err => {
              console.log(err);
              loading.dismiss();
            });

  }
  getImageUrl(name: string){
    return('http://134.209.55.25/static/images/noticias/' + name);
  }

  getHashtags(cuerpo: string) {
    let i = 0;
    const result = [];
    const words = cuerpo.split(' ');
    for (const word of words) {
      if (this.newsService.isHashtagWord(word) && this.newsService.isNotWordInArray(word, result)) {
        // console.log(word);
        result[i] = word;
        i++;
      }
    }
    return result;
  }

}
