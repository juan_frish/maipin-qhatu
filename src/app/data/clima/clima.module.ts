import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ClimaPage } from './clima.page';

const routes: Routes = [
  {
    path: 'lista',
    component: ClimaPage,
    children: [
    { path: 'actual', loadChildren: './actual/actual.module#ActualPageModule' },
    { path: 'historico', loadChildren: './historico/historico.module#HistoricoPageModule' }
    ]
  },
  {
    path: '',
    redirectTo: 'lista/actual'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ClimaPage]
})
export class ClimaPageModule {}
