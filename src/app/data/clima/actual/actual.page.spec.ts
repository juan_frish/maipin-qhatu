import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualPage } from './actual.page';

describe('ActualPage', () => {
  let component: ActualPage;
  let fixture: ComponentFixture<ActualPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
