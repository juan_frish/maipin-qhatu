import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/service/register.service';
import { ApiService } from 'src/app/service/api.service';
import { LoadingController, Platform } from '@ionic/angular';


const CIUDAD = 'ciudad';
const ID = 'Id';
const DATA = 'data';

@Component({
  selector: 'app-actual',
  templateUrl: './actual.page.html',
  styleUrls: ['./actual.page.scss']
})
export class ActualPage implements OnInit {
  ciudad: any;
  data: any;
  FLAG = false;
  width: number;

  constructor(
    private registerService: RegisterService,
    private api: ApiService,
    private loadingController: LoadingController,
    private platform: Platform,
  ) {}

  ngOnInit() {
    this.registerService.getUserInfo().then(res => {
      this.ciudad = res[CIUDAD];
      this.getClimas(res[CIUDAD][ID]);
    });
    this.width = +this.platform.width() * 1.12;
    console.log(this.width);
  }

  async getClimas(id: string) {
    const loading = await this.loadingController.create({
      message: 'Cargando',
      spinner: 'bubbles',
    });
    await loading.present();

    this.api.getClimaActual(id).subscribe(
      res => {
        console.log(res);
        this.data = res[DATA][0];
        this.FLAG = true;
        console.log(this.FLAG);
        loading.dismiss();
      },
      err => {
        console.log(err);
        loading.dismiss();
      }
    );
  }

  getIconUrl(name: string) {
    return 'assets/weather/' + name + '.png';
  }

  getTemperatura(tempMax: any, tempMin: any) {
    const result = (+tempMax + +tempMin) / 2;
    return result;
  }
  getTemperaturaMain(tempMax: any, tempMin: any) {
    const result = (+tempMax + +tempMin) / 2;
    return Math.round(result);
  }
  concat(fecha: string, hora: string){
    return fecha + ' ' + hora;
  }
}
