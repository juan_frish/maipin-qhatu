import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/service/productos.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';

const PRODUCTOS = 'productos';
const DATA = 'data';
const ID = 'Id';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.page.html',
  styleUrls: ['./detalle-producto.page.scss'],
})
export class DetalleProductoPage implements OnInit {

  producto: any;
  FLAG = true;
  FAVORITOS = false;
  favoritos: any[];

  constructor(
    private productosService: ProductosService,
    private activatedRouter: ActivatedRoute,
    private loadingController: LoadingController,
    private alertController: AlertController) { }

  ngOnInit() {
    const getLocation = async () => {
      await  this.getFavoritos();
    };
    getLocation();
    this.activatedRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('idProducto')) {
        // redirect
        return;
      }
      this.getProducto(paramMap.get('idProducto'));
    });
  }

  getImageUrl(name: string) {
    return('http://134.209.55.25/static/images/' + name);
  }

  async getProducto(id: string) {
    const loading = await this.loadingController.create({
      message: 'Cargando',
      spinner: 'bubbles',
    });
    await loading.present();

    this.productosService.getProductos().then( res => {

      this.FLAG = false;
    // tslint:disable-next-line: triple-equals
      this.producto = res[PRODUCTOS].filter( x => x[ID] == id)[0];
      loading.dismiss();
    });
  }

  async getFavoritos() {
    const loading = await this.loadingController.create({
      message: 'Cargando',
      spinner: 'bubbles',
    });
    await loading.present();

    this.productosService.getFavoritos().then( res => {
      this.favoritos = res;
      if (res != null) {
        // this.presentAlert('Hola', 'El tamaño es de ' + this.favoritos.length);
        // console.log(res);
      }
      this.FAVORITOS = true;
      loading.dismiss();
    });
  }
  addFavoritos(id: any, producto: string) {
    this.productosService.addFavoritos(id, producto);
    this.getFavoritos();
  }
  removeFavoritos(id: any) {
    this.productosService.removeFavoritos(id);
    this.getFavoritos();
  }
  esFavorito(id: string) {
    if (this.FAVORITOS ) {
      if (this.favoritos.length > 0) {
        const index: number = this.favoritos.findIndex(x => x.data.Id == id);
        return index !== -1 ? true : false;
      }
      return false;
    }
  }

  async presentAlert(info: string, info2: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: info,
      message: info2,
      buttons: ['OK']
    });

    await alert.present();
  }
}
