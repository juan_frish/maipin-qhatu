import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { ProductosService } from 'src/app/service/productos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.page.html',
  styleUrls: ['./todos.page.scss'],
})
export class TodosPage implements OnInit {

  data: any;
  FLAG = true;

  constructor(
    private loadingController: LoadingController,
    private api: ApiService,
    private productoService: ProductosService
  ) { }

  ngOnInit() {
    this.getAsyncProducts();
  }
  async getAsyncProducts() {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getProductos().subscribe( res => {
      this.data = res;
      this.FLAG = false;
      this.productoService.saveProductos(res);
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });

  }

  getImageUrl(name: string) {
    return('http://134.209.55.25/static/images/' + name);
  }
}
