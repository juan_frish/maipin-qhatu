import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductosPage } from './productos.page';

const routes: Routes = [
  {
    path: 'lista',
    component: ProductosPage,
    children: [
      {
        path: 'todos',
        children: [
          { path: '', loadChildren: './todos/todos.module#TodosPageModule' },
          { path: ':idProducto', loadChildren: './todos/detalle-producto/detalle-producto.module#DetalleProductoPageModule' },
        ]
      },
      { path: 'favoritos', loadChildren: './favoritos/favoritos.module#FavoritosPageModule' },
    ]
  },
  {
    path: '',
    redirectTo: 'lista/todos'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductosPage]
})
export class ProductosPageModule {}
