import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/service/productos.service';
import { LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.page.html',
  styleUrls: ['./favoritos.page.scss'],
})
export class FavoritosPage implements OnInit {

  data: any;
  FLAG = true;
  favoritos: any[];

  constructor(
    private productosService: ProductosService,
    private loadingController: LoadingController,
    private api: ApiService) { }

  ngOnInit() {
    this.getAsyncProducts();
    this.getFavoritos();
  }
  ionViewWillEnter() {
    this.getFavoritos();
  }

  getImageUrl(name: string) {
    return('http://134.209.55.25/static/images/' + name);
  }
  async getAsyncProducts() {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getProductos().subscribe( res => {
      this.data = res;
      this.FLAG = false;
      this.productosService.saveProductos(res);
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });

  }
  async getFavoritos() {
    const loading = await this.loadingController.create({
      message: 'Cargando',
      spinner: 'bubbles',
    });
    await loading.present();

    this.productosService.getFavoritos().then( res => {
      this.favoritos = res;
      console.log(this.favoritos);
      loading.dismiss();
    });
  }

  removeFavoritos(id: string) {
    console.log(id);
    this.productosService.removeFavoritos(id);
    this.getFavoritos();
  }
  noHayFavoritos() {
      if (!this.FLAG ) {
        return this.favoritos.length > 0 ? false : true;
      }
  }

  getFavoritosProducto(id: string) { return this.favoritos.filter( x => x.producto === id); }
}
