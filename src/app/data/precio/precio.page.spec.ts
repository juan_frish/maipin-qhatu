import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecioPage } from './precio.page';

describe('PrecioPage', () => {
  let component: PrecioPage;
  let fixture: ComponentFixture<PrecioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrecioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
