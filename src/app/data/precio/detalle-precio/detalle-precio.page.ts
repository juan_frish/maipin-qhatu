import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { PrecioService } from 'src/app/service/precio.service';

const PRECIOS = 'precios';


@Component({
  selector: 'app-detalle-precio',
  templateUrl: './detalle-precio.page.html',
  styleUrls: ['./detalle-precio.page.scss'],
})
export class DetallePrecioPage implements OnInit {

  variedad: any;
  data: any;
  precio: any;

  BUSCAR = false;
  FLAG = false;
  id = null;
  selected = 0;

  constructor(
    private loadingController: LoadingController,
    private api: ApiService,
    private precioService: PrecioService,
    private modalController: ModalController,
    private navParam: NavParams
  ) { }

  ngOnInit() {
    this.id = this.navParam.get('idVariedad');
    this.getAsyncPrecios(this.id);
  }


 async getAsyncPrecios(id: string) {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getPrecioVariedad(id).subscribe( res => {
      console.log(res);
      this.data = res;
      this.precio = res[PRECIOS][this.selected];
      this.FLAG = true;
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  closeModal() { this.modalController.dismiss(); }

  getMapper() {return this.precioService.getMapper();  }

  changeSelected(id: number) {
    this.selected = id;
    this.getAsyncPrecios(this.id);
  }
}
