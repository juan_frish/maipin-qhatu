import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePrecioPage } from './detalle-precio.page';

describe('DetallePrecioPage', () => {
  let component: DetallePrecioPage;
  let fixture: ComponentFixture<DetallePrecioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePrecioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePrecioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
