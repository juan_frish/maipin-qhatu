import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { ProductosService } from 'src/app/service/productos.service';
import { PrecioService } from 'src/app/service/precio.service';
import { DetallePrecioPage } from '../detalle-precio/detalle-precio.page';



@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {
  producto: any;
  variedad: any;
  data: any;
  data_precios: any;
  PRODUCTO = true;
  VARIEDAD = true;
  BUSCAR = false;
  FLAG = false;

  constructor(
    private loadingController: LoadingController,
    private api: ApiService,
    private productosService: ProductosService,
    private precioService: PrecioService,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.getAsyncProducts();
  }

  async getAsyncProducts() {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getProductos().subscribe( res => {
      this.data = res;
      this.PRODUCTO = false;
      this.productosService.saveProductos(res);
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });

  }
  onProductoChange() { this.VARIEDAD = false; }
  onVariedadChange() { this.BUSCAR = true; }

 getImageUrl(name: string) { return('http://134.209.55.25/static/images/' + name); }

 async getAsyncPrecios(id: string) {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getPrecioVariedad(id).subscribe( res => {
      this.data_precios = res;
      this.FLAG = true;
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  buscar() {
    if (this.BUSCAR) { this.getAsyncPrecios(this.variedad.Id); }
  }
  getMapper() {return this.precioService.getMapper();  }

  async verDetalles(id: string) {
    console.log(id);
    const modal = await this.modalController.create({
      component: DetallePrecioPage,
      componentProps: {
        idVariedad: id,
      }
    });
    await modal.present();
  }
}
