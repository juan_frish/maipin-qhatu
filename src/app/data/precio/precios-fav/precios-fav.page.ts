import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/service/productos.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { PrecioService } from 'src/app/service/precio.service';
import { registerLocaleData } from '@angular/common';
import localeEsPe from '@angular/common/locales/es-PE';
import localEsPeExtra from '@angular/common/locales/extra/es-PE';
import { DetallePrecioPage } from '../detalle-precio/detalle-precio.page';
registerLocaleData(localeEsPe, localEsPeExtra);

@Component({
  selector: 'app-precios-fav',
  templateUrl: './precios-fav.page.html',
  styleUrls: ['./precios-fav.page.scss'],
})
export class PreciosFavPage implements OnInit {

  data: any;
  data_precios: any;
  mapper: any [];
  FLAG = true;
  favoritos: any[];
  select = '';

  constructor(
    private productosService: ProductosService,
    private loadingController: LoadingController,
    private api: ApiService,
    private precioService: PrecioService,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.getFavoritos();
    this.getAsyncProducts();
  }
  async getAsyncProducts() {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getProductos().subscribe( res => {
      this.data = res;
      this.FLAG = false;
      this.productosService.saveProductos(res);
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });

  }
  async getFavoritos() {
    const loading = await this.loadingController.create({
      message: 'Cargando',
      spinner: 'bubbles',
    });
    await loading.present();

    this.productosService.getFavoritos().then( res => {
      this.favoritos = res;
      console.log(this.favoritos);
      loading.dismiss();
    });
  }
  async getAsyncPrecios(id: string) {
    const loading = await this.loadingController.create({
            message: 'Cargando',
            spinner: 'bubbles',
          });
    await loading.present();

    this.api.getPrecioVariedad(id).subscribe( res => {
      this.data_precios = res;
      this.select = id;
      console.log(res);
      loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });

  }
  getImageUrl(name: string) { return('http://134.209.55.25/static/images/' + name); }

  getFavoritosProducto(id: string) { return this.favoritos.filter( x => x.producto === id); }

  noHayFavoritos() {
    if (!this.FLAG ) {
      return this.favoritos.length > 0 ? false : true;
    }
  }

  estaSeleccionado(id: string) { return this.select === id ? true : false; }

  getMapper() {return this.precioService.getMapper();  }

  async verDetalles(id: string) {
    // console.log(id);
    const modal = await this.modalController.create({
      component: DetallePrecioPage,
      componentProps: {
        idVariedad: id,
      }
    });
    await modal.present();
  }

}
