import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreciosFavPage } from './precios-fav.page';

describe('PreciosFavPage', () => {
  let component: PreciosFavPage;
  let fixture: ComponentFixture<PreciosFavPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreciosFavPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreciosFavPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
