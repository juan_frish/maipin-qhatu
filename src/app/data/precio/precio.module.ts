import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PrecioPage } from './precio.page';

const routes: Routes = [
  {
    path: 'lista',
    component: PrecioPage,
    children: [
      { path: 'buscar', loadChildren: './buscar/buscar.module#BuscarPageModule' },
      { path: 'precios-fav', loadChildren: './precios-fav/precios-fav.module#PreciosFavPageModule' },
    ]
  },
  {
    path: '',
    redirectTo: 'lista/precios-fav'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PrecioPage]
})
export class PrecioPageModule {}
