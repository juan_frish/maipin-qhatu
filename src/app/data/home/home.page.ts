import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/service/register.service';

const NOMBRE = 'nombre';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {
  FLAG = true;

  nombre: string;

  constructor(private registerService: RegisterService) {
  }

  ngOnInit() {
    this.registerService.getUserInfo().then(res => {
      console.log(res);
      this.nombre = res[NOMBRE];
      this.FLAG = false;
    });
  }
  ionViewWillEnter() {
    this.registerService.getUserInfo().then(res => {
      console.log(res);
      this.nombre = res[NOMBRE];
      this.FLAG = false;
    });
  }

}
