import { Component, OnInit } from "@angular/core";
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl
} from "@angular/forms";
import { RegisterService } from "src/app/service/register.service";
import { LoadingController } from "@ionic/angular";
import { ApiService } from "src/app/service/api.service";
import { __await } from "tslib";
import { async } from "q";

const DEPARTAMENTO = "departamento";
const PROVINCIA = "provincia";

@Component({
  selector: "app-editar",
  templateUrl: "./editar.page.html",
  styleUrls: ["./editar.page.scss"]
})
export class EditarPage implements OnInit {
  // variables
  editarForm: FormGroup;
  locationForm: FormGroup;

  departamentos: any;
  provincias: any;
  ciudades: any;
  usuario: any;
  FLAG = true;

  // constantes
  DEP_FLAG = true;
  PROV_FLAG = true;
  CIUDAD_FLAG = true;

  constructor(
    public formBuilder: FormBuilder,
    private registerService: RegisterService,
    private loadingController: LoadingController,
    private api: ApiService
  ) {
    this.editarForm = formBuilder.group({
      nombre: [
        '',
        Validators.compose([
          Validators.maxLength(30),
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required,
        ]),
      ],
      departamento: ['', Validators.compose([Validators.required])],
      provincia: ['', Validators.compose([Validators.required]), ],
      ciudad: ['', Validators.compose([Validators.required]), ],
    });
  }

  ngOnInit() {
    // this.getAsyncDepartamentos();

    this.registerService.getUserInfo().then(res => {
      // console.log(res);
      this.usuario = res;
      this.FLAG = false;
      this.editarForm.patchValue({
        nombre: this.usuario.nombre,
        departamento: this.usuario.departamento,
        provincia: this.usuario.provincia,
        ciudad: this.usuario.ciudad
      });

    });
    const getLocation = async () => {
      await this.getAsyncDepartamentos();
      await this.getAsyncProvincias(this.usuario.departamento);
      await this.getAsyncCiudades(this.usuario.provincia);
    };
    getLocation();
  }

  onDepartamentoChange() {
    this.getAsyncProvincias(this.editarForm.value.departamento);
    this.CIUDAD_FLAG = true;
  }

  onProvinciaChange() {
    this.getAsyncCiudades(this.editarForm.value.provincia);
  }

  register() {
    // this.editarForm.value.departamento === '' ?  this.editarForm.value.departamento = this.usuario.departamento : console.log('cul');
    // this.editarForm.value.provincia === '' ?  this.editarForm.value.provincia = this.usuario.provincia : console.log('Provincia perron');
    // this.editarForm.value.ciudad === '' ?  this.editarForm.value.ciudad = this.usuario.ciudad : console.log('Ciudad perros');

    if (this.editarForm.valid) {
      console.log(this.editarForm.value.ciudad);
      this.registerService.register(
        this.editarForm.value.nombre,
        this.editarForm.value.ciudad,
        this.editarForm.value.provincia,
        this.editarForm.value.departamento
      );
    } else {
      console.log(this.editarForm);
      console.log(this.editarForm.value.departamento);
    }
  }

  // Manage Departamentos
  async getAsyncDepartamentos() {
    const loading = await this.loadingController.create({
      message: "Cargando",
      spinner: "bubbles"
    });
    await loading.present();

    this.api.getDepartamentos().subscribe(
      res => {
        this.departamentos = res;
        loading.dismiss();
        this.DEP_FLAG = false;
      },
      err => {
        console.log(err);
        loading.dismiss();
      }
    );
  }

  async getAsyncProvincias(id: string) {
    const loading = await this.loadingController.create({
      message: "Cargando",
      spinner: "bubbles"
    });
    await loading.present();

    this.api.getProvincias(id).subscribe(
      res => {
        this.provincias = res;
        loading.dismiss();
        this.PROV_FLAG = false;
      },
      err => {
        console.log(err);
        loading.dismiss();
      }
    );
  }

  async getAsyncCiudades(id: string) {
    const loading = await this.loadingController.create({
      message: "Cargando",
      spinner: "bubbles"
    });
    await loading.present();

    this.api.getCiudades(id).subscribe(
      res => {
        this.ciudades = res;
        console.log(res);
        loading.dismiss();
        this.CIUDAD_FLAG = false;
      },
      err => {
        console.log(err);
        loading.dismiss();
      }
    );
  }

  compareById(o1, o2) {
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.Id === o2.Id;
  }
}
