import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/service/register.service';
import { ProductosService } from 'src/app/service/productos.service';

const NOMBRE = 'nombre';
const APELLIDO = 'apellido';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  usuario: any;
  FLAG = true;

  constructor(private registerService: RegisterService, private productoService: ProductosService) { }

  ngOnInit() {
    this.registerService.getUserInfo().then(res => {
      this.usuario = res;
      this.FLAG = false;
    });
  }

  getMapper() { return this.registerService.getMapper(); }

  eliminarRegistro() {
    this.registerService.unregister();
    this.productoService.removeAllFav();
  }
}
