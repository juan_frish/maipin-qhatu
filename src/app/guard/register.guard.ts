import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { RegisterService } from '../service/register.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterGuard implements CanActivate {

  constructor(private registerService: RegisterService) {}

  canActivate(): boolean {
    return this.registerService.isRegister();
  }
}
